# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""Utilities for interacting with openmw.cfg style files"""

import fnmatch
import os
from typing import Dict, List


class CommentedLine(str):
    comment: List[str]


# Returns config file as a list of strings (one string per line)
def read_config(path: str) -> List[str]:
    config: List[str] = []
    if os.path.exists(path):
        with open(path, mode="r") as config_file:
            comment = None
            for line in config_file.read().splitlines():
                if not line.strip():
                    continue
                if line.strip().startswith("#"):
                    if comment:
                        comment.append(line)
                    else:
                        comment = [line]
                elif comment:
                    commented_line = CommentedLine(line)
                    commented_line.comment = comment
                    config.append(commented_line)
                    comment = None
                else:
                    config.append(line)
            if comment:
                # Append extra comments to the end of the file
                config.extend(comment)
    return config


# Replaces config file with the given
def write_config(path, new_config):
    # Ensure parents exist
    os.makedirs(os.path.dirname(path), exist_ok=True)
    new_config_sections: Dict[str, List[str]] = {}
    loose_comments = []
    for line in new_config:
        if "=" in line:
            section, _ = line.split("=", maxsplit=1)
            if section in new_config_sections:
                new_config_sections[section].append(line)
            else:
                new_config_sections[section] = [line]
        else:
            loose_comments.append(line)

    with open(path, mode="w") as config:
        # Reverse sort by section to match how openmw-launcher orders sections
        for section in reversed(sorted(new_config_sections.keys())):
            for line in new_config_sections[section]:
                if isinstance(line, CommentedLine):
                    for comment in line.comment:
                        print(comment, file=config)

                print(line, file=config)
        for line in loose_comments:
            print(line, file=config)


def add_config(config, key, entry, index=-1):
    # Remove old entry
    remove_config(config, key)

    if index == -1:
        config.append(entry)
    else:
        config.insert(index, entry)


def check_config(config, key):
    """
    Checks if config contains a line matching the parameters.
    Supports globbing in the prefix and name
    """
    for line in config:
        if fnmatch.fnmatch(line, key):
            return config.index(key)
    return -1


def find_config(config, key):
    """
    Returns index-value pairs for lines in config that match the given key
    """
    lines = []
    for index, line in enumerate(config):
        if fnmatch.fnmatch(line, key):
            lines.append((index, line))
    return lines


def Diff(li1, li2):
    li_dif = [i for i in li1 + li2 if i not in li1 or i not in li2]
    return li_dif


def remove_config(config, key):
    """
    Removes lines from the config matching the parameters.
    Supports globbing in the prefix and name
    """
    to_remove = find_config(config, key)
    # delete in reverse order to preserve indexes
    for index, line in sorted(to_remove, key=lambda x: x[0], reverse=True):
        del config[index]
    return to_remove
