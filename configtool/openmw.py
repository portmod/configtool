# Copyright 2019-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from tempfile import gettempdir
from typing import Dict, List

from portmodlib.fs import ci_exists, is_parent

from . import get_sorted_files, get_vfs_dirs
from .archives import extract_archive_file, list_archive


def _extract_archive_file_to_tmp(archive: str, file: str) -> str:
    """Extracts the given file from the archive and places it in a temprorary directory"""
    temp = gettempdir()
    output_dir = os.path.join(
        temp, ".archive_files", os.path.basename(archive), os.path.dirname(file)
    )
    os.makedirs(output_dir, exist_ok=True)
    result_file = os.path.join(output_dir, os.path.basename(file))
    extract_archive_file(archive, file, output_dir)
    if not os.path.exists(result_file):
        raise Exception(
            'Attempted to extract file "{file}" but destination file "{result_file}" does not exist!'
        )
    return result_file


def find_file(name: str) -> str:
    """
    Locates the path of a file within the OpenMW virtual file system

    args:
        name: The relative path within the VFS to search for

    returns:
        The absolute path of the file
    """
    for directory in reversed(get_vfs_dirs()):
        path = ci_exists(os.path.join(directory, name))
        if path:
            return path

    for archive in reversed(get_sorted_files("ARCHIVES")):
        path = find_file(archive)
        contents = list_archive(path)
        for file in contents:
            if os.path.normpath(file).lower() == os.path.normpath(name).lower():
                return _extract_archive_file_to_tmp(path, file)

    raise FileNotFoundError(name)


def list_dir(name: str) -> List[str]:
    """
    Locates all path of files matching the given pattern within the OpenMW
    virtual file system

    .. warning::
        This function has been deprecated as of Portmod 2.4.
        It will be removed in Portmod 3.0

    args:
        name: The relative path of the directory within the VFS
    returns:
        A list of files contained within the directory
    """
    files: Dict[str, str] = {}
    normalized = os.path.normpath(name).lower()

    for directory in reversed(get_vfs_dirs()):
        path = ci_exists(os.path.join(directory, normalized))
        if path:
            for file in os.listdir(path):
                if file.lower() not in files:
                    files[file.lower()] = file

    for archive in reversed(get_sorted_files("ARCHIVES")):
        contents = list_archive(archive)
        for file in contents:
            if is_parent(os.path.normpath(file).lower(), normalized):
                suffix = os.path.relpath(os.path.normpath(file).lower(), normalized)
                component, _, _ = suffix.partition(os.sep)
                files[component] = component

    return sorted(files.values())
